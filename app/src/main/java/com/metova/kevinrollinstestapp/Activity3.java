package com.metova.kevinrollinstestapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class Activity3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_3);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Disable the button pointing to this activity.
        Button b = (Button) findViewById(R.id.button_3);
        b.setEnabled(false);
    }

    //Intent to switch to Activity 1
    public void goToActivity(View view)
    {
        MyActivity.goToActivity(this, view, getButtonNames());
    }

    public String[] getButtonNames()
    {
        String[] buttons = new String[MyActivity.NUM_BUTTONS];

        buttons[0] = getString(R.string.to_activity_1);
        buttons[1] = getString(R.string.to_activity_2);
        buttons[2] = getString(R.string.to_activity_3);
        return buttons;
    }

}
