package com.metova.kevinrollinstestapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class MyActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.metova.kevinrollinstestapp.MESSAGE";
    public static final String ACTIVITY_1 = "com.metova.kevinrollinstestapp.ACTIVITY_1";
    public static final String ACTIVITY_2 = "com.metova.kevinrollinstestapp.ACTIVITY_2";
    public static final String ACTIVITY_3 = "com.metova.kevinrollinstestapp.ACTIVITY_3";
    public static final int NUM_BUTTONS = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_my, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Called when the user clicks the send button
     * @param view The view that was clicked
     */
    public void sendMessage(View view)
    {
        //Do something in response to button
        Intent intent = new Intent(this, DispayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.edit_message);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public void goToActivity1(View view)
    {
        Intent intent = new Intent(this, Activity1.class);
        startActivity(intent);

    }

    public static String determineActivity(View view, String[] buttons)
    {
        Button b = (Button) view;

        String str = "";
        str = b.getText().toString();

        String activity = "";

        if (str.equals(buttons[0]))
            activity = ACTIVITY_1;
        else if (str.equals(buttons[1]))
            activity = ACTIVITY_2;
        else if (str.equals(buttons[2]))
            activity = ACTIVITY_3;
        else
            activity = "";

        return activity;
    }

    public static void goToActivity(Context context, View view, String[] buttons)
    {
        String activity = determineActivity(view, buttons);
        Class<?> cls;

        if (activity.equals(ACTIVITY_1))
            cls = Activity1.class;
        else if (activity.equals(ACTIVITY_2))
            cls = Activity2.class;
        else if (activity.equals(ACTIVITY_3))
            cls = Activity3.class;
        else
            cls = MyActivity.class;

        Intent intent = new Intent(context, cls);
        context.startActivity(intent);
    }


}
